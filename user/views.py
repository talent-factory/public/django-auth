from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string

from .forms import UserRegistrationForm, UserChangeForm, ChangePasswordForm, ChangeEmailForm
from .token import account_activation_token, user_state_token, email_confirm_token

User = get_user_model()


def home(request):
    count = User.objects.count()
    return render(request, 'home.html', {'count': count})


@login_required()
def signup(request):
    return redirect("home")


def signup(request):
    """
    View for signup.
    If POST: crate new user and send confirmation-link per email.
    :param request: request from user
    :return: rendered view for signup
    """
    # If this is a POST request then process the Form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):
        form = UserRegistrationForm(request.POST)

        # Check if the form is valid:
        if form.is_valid():
            user = User.objects.create_user(
                firstname=form.cleaned_data['firstname'],
                lastname=form.cleaned_data['lastname'],
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password1'],
                phone_mobile=form.cleaned_data['phone_mobile'],
            )
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Please activate your account.'
            message = render_to_string('registration/account_confirm_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()
            messages.success(request, 'We have send you an email to activate your account.')
            return redirect('home')
    else:
        form = UserRegistrationForm()
    return render(request, 'registration/signup.html', {'form': form})


def signup_activate(request, uidb64, token):
    """
    View for confirmation of signup.
    Verify token for confirmation.
    :param request: request from user
    :param uidb64: user_id in base64
    :param token: token to confirm
    :return: redirect to login
    """
    try:
        user_id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=user_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.email_confirmed = True
        user.save()
        messages.success(request, 'Your account is now activated.')
        return redirect('login')
    else:
        messages.warning(request, 'Your link is invalid or expired.')
        return redirect('login')


@login_required()
def change_password(request):
    """
    Change password.
    View vor changing users password.
    :param request: request from user
    :return: rendered view for changing password
    """
    user = get_user(request)
    change_password_form = ChangePasswordForm(user=user)
    if request.method == 'POST':
        change_password_form = ChangePasswordForm(request.POST, user=user)
        if change_password_form.is_valid():
            user.set_password(change_password_form.cleaned_data['password1'])
            user.save()
            update_session_auth_hash(request, user)
            messages.success(request, _('Password successfully changed.'))
            return redirect('user_settings')
    return render(request, 'users/change_password.html', {'form': change_password_form})


@login_required()
def change_email(request):
    """
    Change email.
    View for changing users email.
    :param request: request from user
    :return: rendered view for changing email
    """
    user = get_user(request)
    change_email_form = ChangeEmailForm()
    if request.method == 'POST':
        change_email_form = ChangeEmailForm(request.POST)
        if change_email_form.is_valid():
            user.email = change_email_form.cleaned_data['email']
            user.email_confirmed = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = _('Please confirm your email.')
            message = render_to_string('users/email_confirm_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': email_confirm_token.make_token(user),
            })
            to_email = change_email_form.cleaned_data['email']
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            messages.success(request, _('Email changed. We sent you an link to confirm your new email.'))
            return redirect('home')
    return render(request, 'users/change_email.html', {'form': change_email_form})


@login_required()
def resend_email_confirm(request):
    """
    Resend email confirm.
    Send new email with confirm-link to user.
    Send only if users email is not confirmed.
    :param request: request from user
    :return: redirect to home
    """
    user = get_user(request)
    if not user.email_confirmed:
        current_site = get_current_site(request)
        mail_subject = _('Please confirm your email.')
        message = render_to_string('users/email_confirm_email.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': email_confirm_token.make_token(user),
        })
        to_email = user.email
        email = EmailMessage(
            mail_subject, message, to=[to_email]
        )
        email.send()
        messages.success(request, _('We sent you a new link to confirm your email.'))
    else:
        messages.warning(request, _('Your email is already confirmed.'))
    return redirect('home')


@login_required()
def confirm_email(request, uidb64, token):
    """
    View for confirming email address.
    :param request: request from user
    :param uidb64: user_id in base64
    :param token: token to confirm
    :return: redirect to home
    """
    try:
        user_id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=user_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and email_confirm_token.check_token(user, token):
        user.email_confirmed = True
        user.save()
        messages.success(request, _('Your email is now confirmed.'))
        return redirect('home')
    else:
        messages.warning(request, _('Your link is invalid or expired.'))
        return redirect('home')


def reset_password(request):
    """
    View for reset_password.
    If POST:
        Send email to user with reset-token.
    :param request:
    :return: render or redirect to reset_password (self)
    """
    if request.method == 'POST':
        email = request.POST.get('email', None)
        try:
            user = User.objects.get(email=email)
            current_site = get_current_site(request)
            mail_subject = _('Reset your password.')
            message = render_to_string('users/reset_password_email.html',
                                       {'user': user,
                                        'domain': current_site.domain,
                                        'uid': urlsafe_base64_encode(force_bytes(user.id)),
                                        'token': reset_password_token.make_token(user)})
            to_mail = email
            email = EmailMessage(mail_subject, message, to=[to_mail])
            email.send()
            messages.success(request, _('Yout got an email from us. Please check you spam-folder if needed.'))
            return redirect('reset_password')
        except User.DoesNotExist:
            messages.error(request, _('There is no account registered with this email.'))
            return redirect('reset_password')
    else:
        form = PasswordResetForm()
    return render(request, 'users/reset_password.html', {'form': form})


def reset_password_confirm(request, uidb64, token):
    """
    View for confirm password reset.
    :param request: request from user
    :param uidb64: user-id as uidb64
    :param token: generated user-token from email
    :return: If token is valid:
                rendered view to confrim password reset
             else:
                redirect to home
    """
    try:
        user_id = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=user_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and reset_password_token.check_token(user, token):
        if request.method == 'POST':
            form = SetPasswordForm(user, request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, _('Your password is successfully changed.'))
                return redirect('login')
            else:
                messages.error(request, _('Please check your inputs. The password is not valid.'))
        form = SetPasswordForm(user)
        return render(request, 'users/reset_password_confirm.html', {'form': form})
    else:
        messages.warning(request, _('Your link is invalid or expired.'))
        return redirect('reset_password')
