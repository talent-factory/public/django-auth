from django.urls import path
from . import views

# URL's for users. Prefix: 'users/'
urlpatterns = [
    path('signup', views.signup, name='signup'),
    path('signup/activate/<uidb64>/<token>/', views.signup_activate, name='signup_activate'),
    path('reset-password', views.reset_password, name='reset_password'),
    path('reset-password/<uidb64>/<token>/', views.reset_password_confirm, name='reset_password_confirm'),
]
