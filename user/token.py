from django.contrib.auth.tokens import PasswordResetTokenGenerator
from user.models import User


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    """
    TokenGenerator for signup-confirmation.
    """

    def _make_hash_value(self, user: User, timestamp: int):
        """
        Create token for signup-confirmation.
        :param user: user-object
        :param timestamp: timestamp for token
        :return: generated token
        """
        return (
                int(user.pk) + int(timestamp) +
                bool(user.is_active)
        )


class EmailConfirmTokenGenerator(PasswordResetTokenGenerator):
    """
    TokenGenerator for email-confirmation.
    """

    def _make_hash_value(self, user: User, timestamp: int):
        """
        Create token for email confirmation.
        :param user: user-object
        :param timestamp: timestamp for token
        :return: genertaed token
        """
        return (
                int(user.pk) + int(timestamp) + bool(user.email_confirmed)
        )


class UserStateTokenGenerator(PasswordResetTokenGenerator):
    """
    TokenGenerator for stripe state unique key.
    """

    def _make_hash_value(self, user: User, timestamp: int):
        """
        Create token for stripe unique key.
        :param user: user-object
        :param timestamp: timestamp for token
        :return: generated token
        """
        account_id = 'NOTHING'
        if user.stripe_account_id is not None:
            account_id = user.stripe_account_id
        return (
                user.firstname + user.lastname + account_id
        )


class ResetPasswordTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        """
        Create token for reset_password.
        :param user: user-object
        :param timestamp: timestamp for token
        :return: generated token
        """
        return (
                str(int(user.pk) + int(timestamp)) +
                str(user.last_login)
        )


account_activation_token = AccountActivationTokenGenerator()
email_confirm_token = EmailConfirmTokenGenerator()
user_state_token = UserStateTokenGenerator()
reset_password_token = ResetPasswordTokenGenerator()
